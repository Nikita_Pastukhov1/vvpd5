"""
Программа расшифровывает введеннное пользователем сообщение 
состоящее из точек и тире.
"""

import sys
import doxypypy


def check_input(cipher):
    """
    Функция проверки ввода.

    Принимает на вход введенный пользователем шифр.
    Проверяет каждый элемент на наличие точки или тире.

    Args: 
        cipher: Шифр.

    Returns:
        Возвращает шифр или 'Impossible to decode'.
    
    Raises: 
        TypeError.
    
    Examples:
        >>> check_input(-.-)
        -.-
        >>> check_input(--2)
        Impossible to decode
        >>> check_input(5)
        Traceback (most recent call last):
            ...
        TypeError: object of type 'int' has no len()
    """
    for i in range(len(cipher)):
        if cipher[i] == "." or cipher[i] == "-":
            continue
        elif len(cipher) == 0:
            cipher = 'Impossible to decode'
        else:
            cipher = 'Impossible to decode'
    return cipher

def ship(cipher):
    """
    Функция расшифровки.

    Принимает на вход шифр. 
    Расшифровывает его если он верный.

    Args:
        cipher: Шифр.
    
    Returns:
        Возвращает расшифровку или 'Impossible to decode'.
    
    Examples:
        >>> ship(---)
        O
        >>> ship(213)
        Impossible to decode
        >>> ship(5)
        Traceback (most recent call last):
            ...
        TypeError: object of type 'int' has no len()
    """
    meowpher = []
    if len(cipher) == 0:
        meowpher = ''
    else:
        if len(cipher) % 3 != 0:
            meowpher = 'Impossible to decode'
        else:
            for i in range(3, len(cipher)+3, 3):
                if cipher[i-3:i] == "---":
                    meowpher.append("O")
                elif cipher[i-3:i] == "--.":
                    meowpher.append("E")
                elif cipher[i-3:i] == "-.-":
                    meowpher.append("W")
                elif cipher[i-3:i] == "-..":
                    meowpher.append("M")
                elif cipher[i-3:i] == ".--":
                    meowpher.append('C')
                elif cipher[i-3:i] == ".-.":
                    meowpher.append('A')
                elif cipher[i-3:i] == "..-":
                    meowpher.append('U')
                elif cipher[i-3:i] == "...":
                    meowpher.append('Q')
            meowpher = "".join(meowpher)
    return meowpher


if __name__ == "__main__":
    cipher = str(input("Введите шифр: "))
    check = check_input(cipher)
    if check == 'Impossible to decode':
        print(check)
    else:
        meowpher = ship(cipher)
        print(meowpher)

# Тест в котором проверяется функцмя провекри ввода
def test1_test():
    assert check_input("---") == ("---")
    assert check_input("--.") == ("--.")
    assert check_input("..2") == ('Impossible to decode')

# Тест в котором проверяется расшифровка
def test2_test():
    assert ship("---") == ("O")
    assert ship("--.") == ("E")
    assert ship("...") == ("Q")
    assert ship("--") == 'Impossible to decode'